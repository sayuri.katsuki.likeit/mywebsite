package Model;

public class Main {

	private  int reviewId;
	private String rating;
	private String title;
	private String review;
	private String upDate;
	private String createDate;
	private String id;

	public Main(String rating, String title, String review,String id) {
		super();
		this.rating = rating;
		this.title = title;
		this.review = review;
		this.id = id;

	}

	public Main(String rating, String title, String review) {
		super();
		this.rating = rating;
		this.title = title;
		this.review = review;

	}



	public Main(int reviewId,String rating, String title, String review, String upDate, String createDate) {
		super();
		this.reviewId = reviewId;
		this.rating = rating;
		this.title = title;
		this.review = review;
		this.upDate = upDate;
		this.createDate = createDate;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public String getUpDate() {
		return upDate;
	}
	public void setUpDate(String upDate) {
		this.upDate = upDate;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}



}
