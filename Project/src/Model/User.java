package Model;
import java.io.Serializable;


	public class User implements Serializable {
		private int id;
		private String loginId;
		private String password;
		private String name;
		private String birthday;
		private String life;
		private String createDate;



		// ログインセッションを保存するためのコンストラクタ
		public User(int id, String name) {
			this.id = id;
			this.name = name;
		}


		// 全てのデータをセットするコンストラクタ
		public User(int id, String loginId, String password, String name,String birthday, String life, String createDate) {
			this.id = id;
			this.loginId = loginId;
			this.name = name;
			this.birthday = birthday;
			this.password = password;
			this.setLife(life);
			this.createDate = createDate;
		}


		//新規登録の際、一時的に保存しておくためのコンストラクタ
		public User(String loginId, String password, String name,String birthday, String life) {
			this.loginId = loginId;
			this.name = name;
			this.birthday = birthday;
			this.password = password;
			this.life = life;
		}

		//確認画面から戻る際、一時的に保存しておくためのコンストラクタ
		public User(String loginId,String name,String birthday, String life) {
			this.loginId = loginId;
			this.name = name;
			this.birthday = birthday;
			this.life = life;
		}

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getLoginId() {
			return loginId;
		}
		public void setLoginId(String loginId) {
			this.loginId = loginId;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getBirthday() {
			return birthday;
		}
		public void setBirthday(String birthday) {
			this.birthday = birthday;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getCreateDate() {
			return createDate;
		}
		public void setCreateDate(String createDate) {
			this.createDate = createDate;
		}


		public String getLife() {
			return life;
		}


		public void setLife(String life) {
			this.life = life;
		}





	}


