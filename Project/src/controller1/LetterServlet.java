package controller1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.MainDao;
import Model.User;

/**
 * Servlet implementation class LetterServlet
 */
@WebServlet("/LetterServlet")
public class LetterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LetterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//		セッションなかったらログアウト
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

//id=1だったらメッセージのセッションを削除、なかったら未読なのでそのまま保持

	String read = (String) session.getAttribute("read");

	if(read==null) {


 MainDao mainDao = new MainDao();
		String a = mainDao.LetterMessageA();
		String b = mainDao.LetterMessageB();

		System.out.println("確認"+b);
		System.out.println("確認"+a);

		 request.setCharacterEncoding("UTF-8");
		request.setAttribute("messageA",a);
		request.setAttribute("messageB",b);

	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/letter.jsp");
				dispatcher.forward(request, response);
			}else {

				String messageA = (String) session.getAttribute("messageA");
				String messageB = (String) session.getAttribute("messageB");
				 request.setCharacterEncoding("UTF-8");
					request.setAttribute("messageA",messageA);
					request.setAttribute("messageB",messageB);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/letter.jsp");
					dispatcher.forward(request, response);

			}
			}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

//		フォームに入力した値をゲットし変数に入れる
		HttpSession session = request.getSession();
		String messageA = (String) request.getAttribute("messageA");
		String messageB = (String)request.getAttribute("messageB");
		String read = (String)request.getAttribute("read");

		  session.removeAttribute("messageA");
		  session.removeAttribute("read");
		  session.removeAttribute("messageB");
		  session.removeAttribute("Random");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
			dispatcher.forward(request, response);

	}

}
