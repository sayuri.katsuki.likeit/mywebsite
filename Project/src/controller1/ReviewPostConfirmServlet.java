package controller1;



import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.MainDao;
import Model.User;

/**
 * Servlet implementation class ReviewPostConfirmServlet
 */
@WebServlet("/ReviewPostConfirmServlet")
public class ReviewPostConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewPostConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//		セッションなかったらログアウト
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/confirm.jsp");
				dispatcher.forward(request, response);
			}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String rating = request.getParameter("rating");
		String title = request.getParameter("title");
		String review = request.getParameter("review");
		String userId = request.getParameter("id");
		//DBに合わせユーザーのIDをint型に変換
		int id = Integer.parseInt(userId);


		MainDao mainDao = new MainDao();
		String characterComments = mainDao.CharacterComments();

		 request.setCharacterEncoding("UTF-8");
		 //投稿に対するキャラクターコメント
			request.setAttribute("characterComments",characterComments);

			mainDao.PostReview(rating,title,review,id);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/ok.jsp");
		dispatcher.forward(request, response);
	}

}
