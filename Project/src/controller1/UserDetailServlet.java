package controller1;



import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.MainDao;
import DAO.UserDao;
import Model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ログインセッションがなかったらログアウトする
		HttpSession session = request.getSession();
	    User u = (User)session.getAttribute("userInfo");
		if(u == null){
			response.sendRedirect("LoginServlet");
			return;
		}


		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// idを確認のため出力
		System.out.println("情報詳細表示に使うIDが入ってるか確認"+id);

		//idを引数にして、idに紐づくユーザ情報を用意しスコープに設置
		UserDao userDao = new UserDao();
		User user = userDao.Detail(id);
		request.setAttribute("userDetail", user);

		//ゆーざー取れてるかコンソールで確認
		System.out.println("user情報取れてる？"+user);

        //プロフに出力するランダムコメントを用意しスコープに設置
		 MainDao mainDao = new MainDao();
				String a = mainDao.ProfileQuestion();
				String b = mainDao.ProfileAnser();
				request.setCharacterEncoding("UTF-8");
				request.setAttribute("Question",a);
				request.setAttribute("Anser",b);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
		dispatcher.forward(request, response);


	}


}
