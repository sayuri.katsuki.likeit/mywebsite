package controller1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.MainDao;
import DAO.UserDao;
import Model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserEditServlet")
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User user = userDao.Detail(id);

		// ユーザ情報をリクエストスコープにセットしてjspにフォワード

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useredit.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		String life = request.getParameter("life");

		UserDao userDao = new UserDao();

		if (name.equals("") || birthday.equals("") || life.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			String id = request.getParameter("id");

			// 確認用：idをコンソールに出力
			System.out.println(id);

			// ユーザ一覧情報を取得
			User user = userDao.Detail(id);

			// ユーザ情報をリクエストスコープにセットしてjspにフォワード

			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useredit.jsp");
			dispatcher.forward(request, response);

		} else if (password.equals("") || password2.equals("")) {

			userDao.UpdateNopassword(loginId, name, birthday, life);

			//フォワード先で使うIDをパラメーターから取得
			String id = request.getParameter("id");
			// 確認用：idをコンソールに出力
			System.out.println("passなしUPDATE後に情報用事のためのID" + id);
			//フォワード先で使う情報を引き出しセット
			User user = userDao.Detail(id);
			request.setAttribute("userDetail", user);
			//ランダムコメントも用意して設置
			MainDao mainDao = new MainDao();
			String a = mainDao.ProfileQuestion();
			String b = mainDao.ProfileAnser();
			request.setCharacterEncoding("UTF-8");
			request.setAttribute("Question", a);
			request.setAttribute("Anser", b);
			//ユーザー詳細情報ページの戻る
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
			dispatcher.forward(request, response);

		} else if (!password.equals(password2)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			String id = request.getParameter("id");

			// 確認用：idをコンソールに出力
			System.out.println(id);

			// ユーザ一覧情報を取得
			User user = userDao.Detail(id);

			// ユーザ情報をリクエストスコープにセットしてjspにフォワード

			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useredit.jsp");
			dispatcher.forward(request, response);

		} else {

			//UPDATEする内容をDAOに渡す
			userDao.Update(loginId, password, name, birthday, life);

			//フォワード先で使うIDをパラメーターから取得
			String id = request.getParameter("id");
			// 確認用：idをコンソールに出力
			System.out.println("通常UPDATE後に情報用事のためのID" + id);
			//フォワード先で使う情報を引き出しセット
			User user = userDao.Detail(id);
			request.setAttribute("userDetail", user);
			//ランダムコメントも用意して設置
			MainDao mainDao = new MainDao();
			String a = mainDao.ProfileQuestion();
			String b = mainDao.ProfileAnser();
			request.setCharacterEncoding("UTF-8");
			request.setAttribute("Question", a);
			request.setAttribute("Anser", b);
			//ユーザー詳細情報ページの戻る
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
			dispatcher.forward(request, response);

		}
	}
}