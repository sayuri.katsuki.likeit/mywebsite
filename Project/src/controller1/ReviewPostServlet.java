package controller1;



import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Main;
import Model.User;

/**
 * Servlet implementation class ReviewPostServlet
 */
@WebServlet("/ReviewPostServlet")
public class ReviewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    /**    * @see HttpServlet#HttpServlet()
     */
    public ReviewPostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//		セッションなかったらログアウト
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		//確認画面jspから戻ってきた際にURLパラメーターから値を取得
		request.setCharacterEncoding("UTF-8");
		String rating = request.getParameter("rating");
		String title = request.getParameter("title");
		String review = request.getParameter("review");

		//確認画面jspからとってきた値をスコープにセットして入力画面に遷移
		if (rating != null) {
			Main main= new Main(rating,title,review);
			request.setAttribute("main",main);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/post.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/post.jsp");
				dispatcher.forward(request, response);
			}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		String rating = request.getParameter("rating");
		String title = request.getParameter("title");
		String review = request.getParameter("comment");

		//空白があれば戻る
		if(rating.equals("")||title.equals("")||review.equals("")) {
			request.setAttribute("errMsg", "記入漏れがあるぬ");
			Main main = new Main(rating,title,review);
			request.setAttribute("main",main);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/post.jsp");
			dispatcher.forward(request, response);
			return;
		}else {

//		reting int?string?

//確認用にスコープに入れる
		Main main = new Main(rating,title,review);
		request.setAttribute("main",main);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/confirm.jsp");
		dispatcher.forward(request, response);
		}

	}

}
