package controller1;



import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.MainDao;
import Model.User;

/**
 * Servlet implementation class ReviewEditconfirmServlet
 */
@WebServlet("/ReviewEditconfirmServlet")
public class ReviewEditconfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewEditconfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//		セッションなかったらログアウト
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editconfirm.jsp");
				dispatcher.forward(request, response);
			}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//editconfirmからとんできた
		String rating = request.getParameter("rating");
		String title = request.getParameter("title");
		String review = request.getParameter("review");
		String id = request.getParameter("id");

		System.out.println("編集確認サーブレットでDAOに使うID取れてるか確認"+id);
		//String id = Integer.parseInt(id1);

		//このIDはString型だよ

		MainDao mainDao = new MainDao();
		mainDao.UpdateReview(rating,title,review,id);


//　　　更新完了後に閲覧ページに戻るので表示に必要なID持ってリダイレクト
		HttpSession session = request.getSession();
	    User u = (User)session.getAttribute("userInfo");
        int userId = u.getId();

		response.sendRedirect("ViewServlet?id="+userId);
	}
	}


