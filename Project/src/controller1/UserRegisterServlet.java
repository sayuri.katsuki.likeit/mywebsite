package controller1;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.UserDao;
import Model.User;

/**
 * Servlet implementation class UserRegisterServlet
 */
@WebServlet("/UserRegisterServlet")
public class UserRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//確認画面jspから戻ってきた際にURLパラメーターから値を取得
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		String life = request.getParameter("life");


		//確認画面jspからとってきた値をスコープにセットして入力画面に遷移
		if (name != null) {
			User user = new User(loginId,name,birthday,life);
			request.setAttribute("user",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
			dispatcher.forward(request, response);
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		//入力された値を取得
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		String life = request.getParameter("life");

		//IDがかぶってないか確認するDAO
		UserDao userDao = new UserDao();
		String check = userDao.Check(loginId);
		String errMsg ="";


         if(check!=null) {
        errMsg += "※このログインIDは既に使われています";
	     }

         if(loginId.equals("")||password.equals("")||password2.equals("")||name.equals("")||birthday.equals("")||life.equals("")) {
	    	 errMsg+= "※入力漏れがあるぬ";
		 }
         if(!password.equals(password2)) {
        	 errMsg+="※パっスわードが違う";
         }
         if(errMsg.length() != 0) {
			 User user = new User(loginId,name,birthday,life);
				request.setAttribute("user",user);
				request.setAttribute("errMsg", errMsg);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
			dispatcher.forward(request, response);
			return;

		 }else {

				User user = new User(loginId,password,name,birthday,life);
				request.setAttribute("user",user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registerconfirm.jsp");
		dispatcher.forward(request, response);
				}
	}

}
