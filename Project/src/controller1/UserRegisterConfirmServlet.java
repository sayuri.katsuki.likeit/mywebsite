package controller1;



import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.UserDao;
import Model.User;

/**
 * Servlet implementation class UserRegisterConfirmServlet
 */
@WebServlet("/UserRegisterConfirmServlet")
public class UserRegisterConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegisterConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//		セッションなかったらログアウト
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

//		フォームに入力した値をゲットし変数に入れる
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		String life = request.getParameter("life");
		User user = new User(loginId,password,name,birthday,life);
		request.setAttribute("user",user);

RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userregister.jsp");
dispatcher.forward(request, response);

		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		String life = request.getParameter("life");


		UserDao userDao = new UserDao();
		userDao.Register(loginId,password,name,birthday,life);

		System.out.println("フォワードしないよなんでなん？");

		// パラメータをログイン用DAOに渡して変数に入れる
		User user = userDao.findByLoginInfo(loginId, password);
		System.out.println("これからログインするuser情報入ってるか確認"+user);
		// セッションにユーザID（ログインIDとは違うよ）とネームをセット
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);

		/* フォワードできないのでリダイレクトに変更
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
		dispatcher.forward(request, response);*/

		response.sendRedirect("MainServlet");




	}

}
