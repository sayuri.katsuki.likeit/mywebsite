package controller1;



import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.MainDao;
import Model.Main;
import Model.User;

/**
 * Servlet implementation class ReviewEditServlet
 */
@WebServlet("/ReviewEditServlet")
public class ReviewEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReviewEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


//		セッションなかったらログアウト
		HttpSession session = request.getSession();
	    User u = (User)session.getAttribute("userInfo");
		if(u == null){
			response.sendRedirect("LoginServlet");
			return;
		}


		request.setCharacterEncoding("UTF-8");

		String review1 = request.getParameter("review");

if (review1 == null) {

		// URLからGETパラメータとしてIDを受け取り値があるか確認
		//request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		System.out.println("編集する記事のid入ってるか確認"+id);

         //IDをもとにDAO経由で記事を検索
		 MainDao mainDao = new MainDao();
	     Main review = mainDao.changeReview(id);

	     //ID取れてるか確認
	     int kakunin = review.getReviewId();
	     System.out.println("編集確認サーブレットがgetでID取れてるか"+kakunin);

			// リクエストスコープに記事内容をセットしてjspにフォワード
			// request.setCharacterEncoding("UTF-8");
			 request.setAttribute("review",review);

		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/edit.jsp");
				dispatcher.forward(request, response);
				return;

			}else{

				String rating = request.getParameter("rating");
				String title = request.getParameter("title");
				String id = request.getParameter("id");
				Main main = new Main(rating,title,review1,id);
				request.setAttribute("review",main);
				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/edit.jsp");
				dispatcher.forward(request, response);

			}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");

				String rating = request.getParameter("rating");
				String title = request.getParameter("title");
				String review = request.getParameter("comment");
				String id = request.getParameter("id");

				//！！！ここからがID取れていない！！！

				System.out.println("ReviewEditServletのPOSTでID"+id);

				//空白があれば戻る
				if(rating.equals("")||title.equals("")||review.equals("")) {
					request.setAttribute("errMsg", "なんか違うみたいよ");
					Main main = new Main(rating,title,review,id);
					request.setAttribute("review",main);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/edit.jsp");
					dispatcher.forward(request, response);
					return;
				}else {

//				reting int?string?

		//確認用にスコープに入れる ID取れてなかった
				Main main = new Main(rating,title,review,id);


				System.out.println("ReviewEditServletのPOSTでビーンズのID"+main.getId());


				request.setAttribute("review",main);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editconfirm.jsp");
				dispatcher.forward(request, response);
				}

	}

}
