package controller1;



import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.User;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//randomに１　あったらランダムせず１のまま　なかったらんダム
//ただし、id=１の場合は既読状態のためランダムをつくる
		//		セッションなかったらログアウト
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
//		フォームに入力した値をゲットし変数に入れる

		String read = (String) session.getAttribute("read");

		if(read == null) {

		Random random = new Random();
       int i = random.nextInt(10);
       request.setCharacterEncoding("UTF-8");
		request.setAttribute("Random",i);
		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
				dispatcher.forward(request, response);
		 }else {

			 //readに値があれば未読なのでリンクは表示させたままレターページでセッションを呼び出す
			 request.setCharacterEncoding("UTF-8");
				request.setAttribute("Random",1);
				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
				dispatcher.forward(request, response);
		 }


	}

	/*
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
//		未読の場合セッションに保存する
		String messageA = request.getParameter("messageA");
		String messageB = request.getParameter("messageB");
		String read = request.getParameter("read");

		HttpSession session = request.getSession();
		session.setAttribute("messageA", messageA);
		session.setAttribute("messageB", messageB);
		session.setAttribute("read", read);
		session.setAttribute("Random", 1);

		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
				dispatcher.forward(request, response);
	}

}
