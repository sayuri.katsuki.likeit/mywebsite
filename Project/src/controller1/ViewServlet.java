package controller1;



import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.MainAverage;
import DAO.MainDao;
import Model.Main;
import Model.User;

/**
 * Servlet implementation class ViewServlet
 */
@WebServlet("/ViewServlet")
public class ViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
	    User u = (User)session.getAttribute("userInfo");

		if(u == null){
			response.sendRedirect("LoginServlet");
			return;
		}

		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");

		MainDao mainDao = new MainDao();
		 List<Main> reviewList = mainDao.reviewAll(id);
		 List<Main> reviewList2 = mainDao.reviewAll(id);
Collections.reverse(reviewList2);

		// 平均を求めるDAOに渡すためにIDをStringからintに変換する
		// int id2 = Integer.parseInt("id");
		 //パラメーターふたつにする
		 //int idq = Integer.parseInt(request.getParameter("idq"));


		 MainAverage mainaverage =new MainAverage();
		 double average = mainaverage.RatingAverage(id);
//
//		 request.setCharacterEncoding("UTF-8");

			request.setAttribute("reviewList",reviewList);
			request.setAttribute("reviewList2",reviewList2);
			request.setAttribute("average",average);
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/view.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
