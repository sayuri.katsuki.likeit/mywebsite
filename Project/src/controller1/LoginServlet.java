package controller1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.UserDao;
import Model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
 public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログイン

		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");

		if (u != null) {
			response.sendRedirect("MainServlet");
			return;
		}

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// 入力されたパラメータを取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

//				値が入ってるかの確認
				System.out.println("login"+loginId);
				System.out.println("password"+password);

		// パラメータをログイン用DAOに渡して変数に入れる
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginInfo(loginId, password);
		System.out.println("これからログインするuser情報入ってるか確認"+user);

		//	もし情報が見つからなかった場合ログイン画面戻り下記のエラメを表示
		if (user == null) {
			request.setAttribute("errMsg", "ログインIDかパスワードが違うかも。新規登録は済んでる？");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// セッションにユーザID（ログインIDとは違うよ）とネームをセット
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);

		// メインサーブレットにリダイレクト
		response.sendRedirect("MainServlet");


	}

}
