package controller1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.MainDao;
import Model.Main;
import Model.User;

/**
 * Servlet implementation class ReviewDeleteServlet
 */
@WebServlet("/ReviewDeleteServlet")
public class ReviewDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReviewDeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//		セッションなかったらログアウト
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");

		if (u == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// URLからGETパラメータとしてIDを受け取る
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");

		System.out.println("削除する記事id入ってるか確認" + id);

		//IDをもとにDAO経由で記事を検索
		MainDao mainDao = new MainDao();
		Main review = mainDao.changeReview(id);

		// リクエストスコープに記事内容をセットしてjspにフォワード
		request.setCharacterEncoding("UTF-8");
		request.setAttribute("review", review);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/delete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String userid = request.getParameter("userid");
		//記事IDをDAOに渡し記事を削除したらJSPにフォワード
		MainDao mainDao = new MainDao();
		mainDao.DeleteReview(id);

	//　　　更新完了後に閲覧ページに戻るので表示に必要なID持ってリダイレクト
			HttpSession session = request.getSession();
		    User u = (User)session.getAttribute("userInfo");
	        int userId = u.getId();

			response.sendRedirect("ViewServlet?id="+userId);
	}

}
