package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Model.Main;

public class MainDao {


	public void PostReview(String rating, String title, String review,int id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//
			//            もとあるDAOと比較して作る

			String sql = "INSERT INTO t_review (title,review,user_id,t_rating,create_date) VALUES (?,?,?,?,NOW())";
			// INSERTを実行

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,title);
			pStmt.setString(2,review);
			pStmt.setInt(3,id);
			pStmt.setString(4,rating);


			int result2 = pStmt.executeUpdate();
			// 追加された行数を出力
			System.out.println("確認"+result2);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	public List<Main> reviewAll(String id) {
		Connection conn = null;
		List<Main> reviewList = new ArrayList<Main>();


		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_review WHERE user_id=?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を
			while (rs.next()) {
				int reviewId = rs.getInt("t_review_id");
				String title = rs.getString("title");
				String review = rs.getString("review");
				String rating = rs.getString("t_rating");
				String createDate = rs.getString("create_date");
				String upDate = rs.getString("update_date");
				Main main = new Main(reviewId,rating,title,review,upDate,createDate);

				reviewList.add(main);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return reviewList;
	}

	public Main changeReview(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 選択した記事を記事IDにより出力
			String sql = "SELECT * FROM t_review WHERE t_review_id=?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();

			//実行結果の内容をmainビーンズに追加
			while (rs.next()) {
				int reviewId = rs.getInt("t_review_id");
				String title = rs.getString("title");
				String review = rs.getString("review");
				String rating = rs.getString("t_rating");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				Main main = new Main(reviewId,rating,title,review,updateDate,createDate);

				System.out.println("DAO：ID取れてるか確認"+reviewId);

				System.out.println("DAO：編集と削除に使うのビーンズに値が入ってるか確認"+main);

				return main;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;

	}


	public void UpdateReview(String rating, String title, String review, String id) {

		Connection conn = null;

		try {

			System.out.println("UPDATEのDAO：引数の値が取れてるか確認"+rating+title+review+id);

			//int id1 = Integer.parseInt(id);

			// データベースへ接続
			conn = DBManager.getConnection();


			String sql = "UPDATE t_review SET title=?,review=?,t_rating=?,update_date=NOW() WHERE t_review_id=?;";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,title);
			pStmt.setString(2,review);
			pStmt.setString(3,rating);
			pStmt.setString(4,id);

			int result = pStmt.executeUpdate();
			System.out.println("UPDATEされた行数を出力確認"+result);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	public void DeleteReview(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//
			//            もとあるDAOと比較して作る

			String sql = "DELETE FROM t_review WHERE t_review_id=?";
			// INSERTを実行

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);

			int result = pStmt.executeUpdate();
			System.out.println("削除した数を出力"+result);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}




	public String LetterMessageA() {
		Connection conn = null;
		String messageA ;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT message_a FROM letter_message_a ORDER BY RAND() LIMIT 1;";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			System.out.println("確認DAO"+rs);

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			 messageA = rs.getString("message_a");

			System.out.println("確認DAO2"+ messageA );

//			String letterDate1 = new LetterDate();
//
//			String messageA = letterDate;
			return messageA;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	public String LetterMessageB() {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT message_b FROM letter_message_b ORDER BY RAND() LIMIT 1;";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String messageB = rs.getString("message_b");


			return messageB;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String ProfileQuestion() {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT question FROM profile_question ORDER BY RAND() LIMIT 1;";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String question = rs.getString("question");

			return question;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String ProfileAnser() {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT anser FROM profile_anser ORDER BY RAND() LIMIT 1;";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String anser = rs.getString("anser");

			return anser;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String CharacterComments() {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT comment FROM character_comments ORDER BY RAND() LIMIT 1;";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String comment = rs.getString("comment");

			return comment;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public String OthersideLetter() {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT message  FROM otherside_letter ORDER BY RAND() LIMIT 1;";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String message  = rs.getString("message ");

			return message;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}



}
