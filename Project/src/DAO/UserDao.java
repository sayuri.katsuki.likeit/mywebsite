package DAO;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import Model.User;



//
//DAOで必要なメソッド
//・ログイン
//・ユーザー情報インサート
//・ユーザー情報セレクト
//・ユーザー情報アップデート
//・記事インサート
//・記事アップデート
//・記事デリート
//・評価インサート
//・評価アップデート
//・評価デリート
//・評価ごと記事出力
//
//
//
//以下ランダム出力するだけ
//・レターメッセージA,B
//・プロフィールクエスチョン、アンサー
//・キャラクターコメント
//・裏ページレター
//

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */

	//ログインIDかぶってないか確認するDAO
	public String Check(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT login_id FROM t_user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			return loginId;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ログインするDAO
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int userIdData = rs.getInt("user_id");
			String nameData = rs.getString("user_name");
			System.out.println("userIdData:"+userIdData);

			return new User(userIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}



	public void Register(String loginId, String password, String name, String birthday, String life) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			//
			//            もとあるDAOと比較して作る

			String sql = "INSERT INTO t_user(login_id,password,user_name,birth_date,life,create_date) VALUES (?,?,?,?,?,NOW())";
			// INSERTを実行

			String source = "password";
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println("パっスワード暗号化した結果"+result);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			pStmt.setString(3, name);
			pStmt.setString(4, birthday);
			pStmt.setString(5, life);
			int result2 = pStmt.executeUpdate();
			// 追加された行数を出力
			System.out.println("登録成功した証"+result2);
			pStmt.close();

		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	public User Detail(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE user_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加

			int Id = rs.getInt("user_id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("user_name");
			String birthDay = rs.getString("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String life = rs.getString("life");
			User user = new User(Id, loginId, password, name, birthDay, life,createDate);

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}

			}
		}

	}

	public void Update(String loginId, String password, String name, String birthday,String life) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE t_user SET password=?,user_name=?,birth_date=?,life=? WHERE login_id=?;";

			//ハッシュを生成したい元の文字列
			String source = "password";
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println("ぱっすぁーどを暗号化したよ～て証"+result);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, password);
			pStmt.setString(2, name);
			pStmt.setString(3, birthday);
			pStmt.setString(4, life);
			pStmt.setString(5, loginId);
			int result2 = pStmt.executeUpdate();
			// 追加された行数を出力
			System.out.println("DAOからDBへユーザー情報をアプデした数"+result2);
			pStmt.close();
		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}



	public void UpdateNopassword(String loginId, String name, String birthday,String life) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "UPDATE t_user SET user_name=?,birth_date=?,life=? WHERE login_id=?;";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthday);
			pStmt.setString(3, life);
			pStmt.setString(4, loginId);
			int result = pStmt.executeUpdate();
			// 追加された行数を出力
			System.out.println("DAOからDBへパっスなしでユーザー情報アプデした数"+result);
			pStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}





}

