package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MainAverage {

	public double RatingAverage(String id) {
		Connection conn = null;
		int ratingNumberOne = 0 ;
		int countNumberOne ;
		int ratingNumberTwo  = 0 ;
		int countNumberTwo ;
		int ratingNumberThree  = 0 ;
		int countNumberThree ;
		String ratingNumber="";

	    //評価☆を数値化する
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//評価を数値に変換
			String sql = "SELECT t_rating FROM t_review WHERE t_rating='one' AND user_id=?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();

			if (rs.next()) {
				// 数値に変換した評価を出力（ワイルじゃなくていいよね？）
				ratingNumber = rs.getString("t_rating");

				//外に宣言した変数に値を入れたい場合IFの当てはまる場合とそうじゃない場合も値を入れる流れにする必要がある
				if(ratingNumber.equals("one")) {
					ratingNumberOne=1;
				}else {
					ratingNumberOne=1;
				}
			}




		//評価☆の合計数


			//評価を数値に変換
			String sql1 = "SELECT count(*) as count_one from t_review where t_rating ='one'AND user_id=?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt1 = conn.prepareStatement(sql1);
			pStmt1.setString(1,id);
			ResultSet rs1 = pStmt1.executeQuery();

			//０でも一行下がるのでreturnしないはず
			if (!rs1.next()) {
				return 0;
			}

			// 数値に変換した評価を出力（ワイルじゃなくていいよね？）
			countNumberOne = rs1.getInt("count_one");





	    //評価☆☆を数値化する

			//評価を数値に変換
			String sql2 = "SELECT t_rating FROM t_review WHERE t_rating='two' AND user_id=?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt2 = conn.prepareStatement(sql2);
			pStmt2.setString(1,id);
			ResultSet rs2 = pStmt2.executeQuery();
			if (rs2.next()) {
			// 数値に変換した評価を出力（ワイルじゃなくていいよね？）
			String ratingNumber2 = rs2.getString("t_rating");

			if(ratingNumber.equals("two")) {
				ratingNumberTwo=2;
			}else {
				ratingNumberTwo=2;
			}
			}


		//評価☆☆の合計数

			//評価を数値に変換
			String sql22 = "SELECT count(*) as count_two from t_review where t_rating ='two'AND user_id=?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt22 = conn.prepareStatement(sql22);
			pStmt22.setString(1,id);
			ResultSet rs22 = pStmt22.executeQuery();

			if (!rs22.next()) {
				return 0;
			}

			// 数値に変換した評価を出力（ワイルじゃなくていいよね？）
			countNumberTwo = rs22.getInt("count_two");




	    //評価☆☆☆を数値化する

			//評価を数値に変換
			String sql3 = "SELECT t_rating FROM t_review WHERE t_rating='three' AND user_id=?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt3 = conn.prepareStatement(sql3);
			pStmt3.setString(1,id);
			ResultSet rs3 = pStmt3.executeQuery();

			if (rs3.next()) {

			// 数値に変換した評価を出力（ワイルじゃなくていいよね？）
			String ratingNumber3 = rs3.getString("t_rating");

			if(ratingNumber.equals("three")) {
				ratingNumberThree=3;
			}else {
				ratingNumberThree=3;
			}
			}


		//評価☆☆☆の合計数

			//評価を数値に変換
			String sql33 = "SELECT count(*) as count_three from t_review where t_rating ='three'AND user_id=?;";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt33 = conn.prepareStatement(sql33);
			pStmt33.setString(1,id);
			ResultSet rs33 = pStmt33.executeQuery();

			if (!rs33.next()) {
				return 0;
			}

			// 数値に変換した評価を出力（ワイルじゃなくていいよね？）
			countNumberThree = rs33.getInt("count_three");




		//☆の合計
		int sumOneScore = ratingNumberOne*countNumberOne;
		//☆☆の合計
		int sumTwoScore = ratingNumberTwo*countNumberTwo ;
		//☆☆☆の合計
		int sumThreeScore = ratingNumberThree*countNumberThree ;

		//評価の合計点
		double totalRatingScore =sumOneScore+sumTwoScore+sumThreeScore;

		//記事合計数
		double totalReview = countNumberOne+countNumberTwo+countNumberThree;

		//平均を出す
		double a = totalRatingScore/totalReview;

		double average = Math.floor(a);

		System.out.println("sumOneScore "+sumOneScore);
		System.out.println(" sumTwoScore"+ sumTwoScore);
		System.out.println("sumThreeScore "+sumThreeScore );
		System.out.println("totalRatingScore"+totalRatingScore);
		System.out.println("totalReview"+totalReview);
		System.out.println(" average"+ average);

		return average;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}

	}

}
