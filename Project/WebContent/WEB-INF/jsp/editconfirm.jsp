<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="jsp/error.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>記事記編集確認</title>
    <!-- オリジナルCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/editconfirm.css" rel="stylesheet">
</head><body>
<section>
    <p></p>
    <p>
    I　Q　３　に　な　っ　て　み　る　？</p>
</section>
       <div class="row">
  <div class="col-md-8 offset-md-2">
<p></p>
      <br>
 <h1><p>自分レビューを編集する</p></h1>
      <br>
   <font face="BIZ UDMincho"> <font color="colar">   <p>以下の内容でよろしいですか？  </p> </font>
      <br>
      <h3><p>${review.title}</p></h3>
       <h5><p><c:if test="${main.rating eq 'three'}">☆☆☆</c:if>
<c:if test="${main.rating eq 'two'}">☆☆</c:if>
<c:if test="${main.rating eq 'one'}">☆</c:if></p>
         <p>${review.review}</p></h5></font>
      <br><h5>
<form action="ReviewEditconfirmServlet" method="post">
    <input type="hidden" name="title" value="${review.title}">
    <input type="hidden" name="rating" value="${review.rating}">
     <input type="hidden" name="review" value="${review.review}">
      <input type="hidden" name="id" value="${review.id}">
    <div class="col-sm-7" align="right">
<input src="image/login.png" class="image_button"  type="image">
<a href="ReviewEditServlet?title=${review.title}&review=${review.review}&rating=${review.rating}&id=${review.id}" class="cp_textlink01">back</a>
</form>
  </div>
               </div>
              </div>
        <p></p>
<section>
 <br>
 <div class="col-sm-12" align="center"><a href="MainServlet" class="cp_textlink01">TOPへ戻る</a></div>
 <div class="col-sm-12" align="center">arigatou-dance-odorow(yo)</div></section>

</section>
    </body>

</html>
