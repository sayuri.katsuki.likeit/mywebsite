<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="jsp/error.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>記事編集</title>
    <!-- オリジナルCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/edit.css" rel="stylesheet">
</head><body>
<section>
    <p></p>
    <p>
    I　Q　３　に　な　っ　て　み　る　？</p>
</section>
       <div class="row">
  <div class="col-md-8 offset-md-2">
<p></p>
 <h1>自分レビューを編集する</h1>
         <form action="ReviewEditServlet" method="post">
  <div class="cp_ipselect cp_sl01">
 <c:if test ="${errMsg != null}">${errMsg}</c:if>
<select name="rating">
	<option value="${review.rating}" hidden><c:choose>
<c:when test="${review.rating eq 'three'}">☆☆☆</c:when>
<c:when test="${review.rating eq 'two'}">☆☆</c:when>
<c:when test="${review.rating eq 'one'}">☆</c:when>
</c:choose></option>
	<option value="three">☆☆☆</option>
	<option value="two">☆☆</option>
	<option value="one">☆</option>
</select>
  <textarea name="title" cols="150" rows="2">${review.title}</textarea>
<br>
    <textarea name="comment" cols="150" rows="30">${review.review}</textarea>
  </div> <input type="hidden" name="id" value="${review.reviewId}">
             <div class="col-sm-7" align="right">
        <input src="image/login.png" class="image_button"  type="image">
                 </div>
 </form>
           </div>
                      </div>
        <p></p>
<section>
<br>
 <div class="col-sm-12" align="center"><a href="MainServlet" class="cp_textlink01">TOPへ戻る</a></div>
 <div class="col-sm-12" align="center">arigatou-dance-odorow(yo)</div></section>

</section>
    </body>

</html>
