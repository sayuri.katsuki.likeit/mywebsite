<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="jsp/error.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>記事削除</title>
    <!-- オリジナルCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/confirm.css" rel="stylesheet">
</head><body>
<section>
    <p></p>
    <p>
    I　Q　３　に　な　っ　て　み　る　？</p>
</section>
       <div class="row">
  <div class="col-md-8 offset-md-2">
<p></p>
      <br>
 <h1><p>自分レビューを削除する</p></h1>
      <br>
   <font face="BIZ UDMincho"> <font color="colar">   <p>以下の内容を削除しますか？  </p> </font>
      <br>
       <h3><p>${review.title}</p></h3>
       <h5><p>
<c:if test="${main.rating eq 'three'}">☆☆☆</c:if>
<c:if test="${main.rating eq 'two'}">☆☆</c:if>
<c:if test="${main.rating eq 'one'}">☆</c:if></p>
         <p>${review.review}</p></h5></font>
      <br><h5>
 <form action="ReviewDeleteServlet" method="post">
    <input type="hidden" name="id" value="${review.reviewId}">

    <div class="col-sm-7" align="right">
    <button type="submit" class="submit_btn">yes</button>
    <a href="ViewServlet?id=${userInfo.id}">no</a>
  </div>   </form>
               </div>
              </div>
        <p></p>
<section>
<br>
 <div class="col-sm-12" align="center"><a href="MainServlet" class="cp_textlink01">TOPへ戻る</a></div>
 <div class="col-sm-12" align="center">arigatou-dance-odorow(yo)</div></section>

</section>
    </body>

</html>
