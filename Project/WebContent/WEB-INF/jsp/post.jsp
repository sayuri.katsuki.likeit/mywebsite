<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="jsp/error.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>投稿</title>
    <!-- オリジナルCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/post.css" rel="stylesheet">
</head><body>
<section>
    <p></p>
    <p>
    I　Q　３　に　な　っ　て　み　る　？</p>
</section>
       <div class="row">
  <div class="col-md-8 offset-md-2">
<p></p>
 <h1>本日の自分レビュー</h1>
  <c:if test ="${errMsg != null}"><font color="#ea5550">${errMsg}</font></c:if>
         <form action="ReviewPostServlet" method="post">
  <div class="cp_ipselect cp_sl01">
<select name="rating">
 <c:choose>
<c:when test ="${rating == null}"><option value="" hidden>☆の数を気分で選んでね。 </option></c:when>
	<c:when test ="${rating != null}"><option value="${main.rating}" hidden></c:when></c:choose>
<c:if test="${main.rating eq 'three'}">☆☆☆</c:if>
<c:if test="${main.rating eq 'two'}">☆☆</c:if>
<c:if test="${main.rating eq 'one'}">☆</c:if>

 </option>		<option value="three">☆☆☆</option>
	<option value="two">☆☆（見られてるみたい）</option>
	<option value="one">☆(オンリーワン)</option>
</select>

タイトル
                <textarea name="title" cols="150" rows="2">${main.title}</textarea>
  内容  <a href="RuleServlet#sample">sample</a><textarea name="comment" cols="150" rows="30"><c:if test ="${main.review != null}">${main.review}</c:if></textarea>
  </div>
             <div class="col-sm-7" align="right">

           <input src="image/login.png" class="image_button"  type="image">
                 </div>
 </form><a href ="RuleServlet"></a>
           </div>
                      </div>
        <p></p>
<section>
<br>
 <div class="col-sm-12" align="center"><a href="MainServlet" class="cp_textlink01">TOPへ戻る</a></div>
 <div class="col-sm-12" align="center">arigatou-dance-odorow(yo)</div></section>
    </body>

</html>
