<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="error.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>メイン</title>
<!-- オリジナルCSS読み込み -->

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/main.css" rel="stylesheet">


</head>

<body>

	<div class="col-sm-12">
		<p></p>
		<p></p>
		<div class="title">
			<p>${userInfo.name}ちゃ ん の 襲 来</p>
		</div>
	</div>
	<div class="outer">
		<div class="inner">
			<c:if test="${Random == 1}">
				<a href="LetterServlet" class="cp_textlink02"><h4>(おてがみ、とどいてるみたいよ)</h4></a>
			</c:if></div></div><div class="outer">
		<div class="inner">
			<div class="row pr">
			<a href="ReviewPostServlet"><img src="image/p.png" class="image_button"></a>
			<a href="ViewServlet?id=${userInfo.id}"><img src="image/review.png" class="image_button"></a>
			</div>
			<div class="row pm">
			<a href="UserDetailServlet?id=${userInfo.id}"><img src="image/pro.png" class="image_button"></a>
			<a href="RuleServlet"><img src="image/mindset.png" class="image_button"></a>
			</div>
			<a href="LogoutServlet" class="cp_textlink02"><h5>roguauto</h5></a>
		</div></div>
	</div>


</body>

</html>
