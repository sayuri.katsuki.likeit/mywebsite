<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="jsp/error.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>メイン</title>
    <!-- オリジナルCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/confirm.css" rel="stylesheet">
</head><body>
<section>
    <p></p>
    <p>
    I　Q　３　に　な　っ　て　み　る　？</p>
</section>
       <div class="row">
  <div class="col-md-8 offset-md-2">
<p></p>
      <br>
 <h1><p>本日の自分レビュー</p></h1>
      <br>
   <font face="BIZ UDMincho"> <font color="colar">   <p>以下の内容でよろしいですか？  </p> </font>
      <br>
       <h3><p>${main.title}</p></h3>
       <h5><p>
<c:if test="${main.rating eq 'three'}">☆☆☆</c:if>
<c:if test="${main.rating eq 'two'}">☆☆</c:if>
<c:if test="${main.rating eq 'one'}">☆</c:if></p>
         <p>${main.review}</p></h5></font>
      <br><h5>
 <form action="ReviewPostConfirmServlet?id=${userInfo.id}" method="post">
    <input type="hidden" name="title" value="${main.title}">
    <input type="hidden" name="rating" value="${main.rating}">
     <input type="hidden" name="review" value="${main.review}">
    <div class="col-sm-7" align="right">
<input src="image/login.png" class="image_button"  type="image">
<a href="ReviewPostServlet?title=${main.title}&review=${main.review}&rating=${main.rating}" class="cp_textlink01">back</a>
  </div>   </form>
               </div>
              </div>
        <p></p>
<section>
<br>
 <div class="col-sm-12" align="center"><a href="MainServlet" class="cp_textlink02">TOPへ戻る</a></div>
 <div class="col-sm-12" align="center">arigatou-dance-odorow(yo)</div></section>

</section>
    </body>

</html>
