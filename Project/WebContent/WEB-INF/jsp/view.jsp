<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="jsp/error.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>評価一覧</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/view.css" rel="stylesheet">
</head>

<body>
	<section>
		<p></p>
		<p>I Q ３ に な っ て み る ？</p>
	</section>
	<br>
	<br>
	<div class="col-md-6 offset-md-3">
		<h3>
			自己評価平均＜＜
			<c:choose>
				<c:when test="${average <= 0.9}">判定不可</c:when>
				<c:when test="${average >= 1.0 and average <= 1.4}"><img src="image/star1.png"></c:when>
				<c:when test="${average >= 1.5 and average <= 1.9}"><img src="image/star15.png"></c:when>
				<c:when test="${average >= 2.0 and average <= 2.4}"><img src="image/star2.png"></c:when>
				<c:when test="${average >= 2.5 and average <= 2.9}"><img src="image/star25.png"></c:when>
				<c:when test="${average == 3.0}"><img src="image/star3.png"></c:when>

			</c:choose>
			＞＞
		</h3>
		<div class="cp_tab">
			<input type="radio" name="cp_tab" id="tab1_1"
				aria-controls="first_tab01" checked> <label for="tab1_1">全評価</label>

			<input type="radio" name="cp_tab" id="tab1_2"
				aria-controls="second_tab01"> <label for="tab1_2">☆</label>

			<input type="radio" name="cp_tab" id="tab1_3"
				aria-controls="third_tab01"> <label for="tab1_3">☆☆</label>
			<input type="radio" name="cp_tab" id="tab1_4"
				aria-controls="force_tab01"> <label for="tab1_4">☆☆☆</label>

			<div class="cp_tabpanels">
				<div id="first_tab01" class="cp_tabpanel">



					<div class="cp_box">
						<input id="cp01" type="checkbox">
						<!--  リンク踏めないので一旦消す<div class="cp_box">-->
						<input type="checkbox"> <label for="cp01"></label>
						<div class="cp_container">
							<c:forEach var="reviewList" items="${reviewList}">
								<br>
								<h2><p></p>

								  <p>
										<font color=#dc143c>"${reviewList.title}"</font>
									</p>
								</h2>
								<p>
									評価
									<c:if test="${reviewList.rating == 'three'}">
										<c:out value="☆☆☆" />
									</c:if>
									<c:if test="${reviewList.rating == 'two'}">
										<c:out value="☆☆" />
									</c:if>
									<c:if test="${reviewList.rating == 'one'}">
										<c:out value="☆" />
									</c:if>
									投稿 ${reviewList.createDate}
									<c:if test="${reviewList.upDate != null}">
										<font color=orange> 更新${reviewList.upDate}</font>
									</c:if>
								</p>
								<h5>
									 <p class="b-keep">${reviewList.review}</p>
								</h5>
								<p>
									<a href="ReviewEditServlet?id=${reviewList.reviewId}">□編集</a><a
										href="ReviewDeleteServlet?id=${reviewList.reviewId}">□削除</a>
								</p>
								<p>＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿</p>
							</c:forEach>

						</div>
					</div>

					<!-- 	<div class="cp_box2">
						<input id="cp02" type="checkbox"> <label for="cp02"></label>
						<div class="cp_container2">
							<br>
							<h2>
								<font color=#dc143c>"生きてるって感じがしました。"</font>
							</h2>
							評価 ☆☆☆ 投稿日時2021/01/23 <font color=orange>2021/01/24更新</font>
							<h5>
								<p>ごはん作った。</p>
								<p>自炊した、俺偉い。がんばった。しかもかなり美味い。 作ったのは、だし巻きたまごとほうれん草の胡麻和えと白米。
								<p>これで彼女いないとか信じられない。</p>
							</h5>
							<a href="ReviewEditServlet?id=${review.reviewId}">□編集</a><a
								href="ReviewDeleteServlet?id=${review.reviewId}">□削除</a>
						</div>
					</div> -->
				</div>
				<div id="second_tab01" class="cp_tabpanel">
					<c:forEach var="reviewList" items="${reviewList}">
						<c:if test="${reviewList.rating == 'one'}">
							<br>
							<h2>
								<font color=#dc143c>"${reviewList.title}"</font>
							</h2>
								 評価
									<c:if test="${reviewList.rating == 'three'}">
								<c:out value="☆☆☆" />
							</c:if>
							<c:if test="${reviewList.rating == 'two'}">
								<c:out value="☆☆" />
							</c:if>
							<c:if test="${reviewList.rating == 'one'}">
								<c:out value="☆" />
							</c:if>
								 投稿 ${reviewList.createDate}
								<c:if test="${reviewList.upDate != null}">
								<font color=orange> 更新${reviewList.upDate}</font>
							</c:if>
							<h5>
								<p>${reviewList.review}</p>
								<p></p>
							</h5>
							<a href="ReviewEditServlet?id=${reviewList.reviewId}">□編集</a>
							<a href="ReviewDeleteServlet?id=${reviewList.reviewId}">□削除</a>
						<p>＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿</p></c:if>
					</c:forEach>
				</div>
				<div id="third_tab01" class="cp_tabpanel">
					<c:forEach var="reviewList" items="${reviewList}">
						<c:if test="${reviewList.rating == 'two'}">
							<br>
							<h2>
								<font color=#dc143c>"${reviewList.title}"</font>
							</h2>
								 評価
									<c:if test="${reviewList.rating == 'three'}">
								<c:out value="☆☆☆" />
							</c:if>
							<c:if test="${reviewList.rating == 'two'}">
								<c:out value="☆☆" />
							</c:if>
							<c:if test="${reviewList.rating == 'one'}">
								<c:out value="☆" />
							</c:if>
								 投稿 ${reviewList.createDate}
								<c:if test="${reviewList.upDate != null}">
								<font color=orange> 更新${reviewList.upDate}</font>
							</c:if>
							<h5>
								<p>${reviewList.review}</p>
								<p></p>
							</h5>
							<a href="ReviewEditServlet?id=${reviewList.reviewId}">□編集</a>
							<a href="ReviewDeleteServlet?id=${reviewList.reviewId}">□削除</a>
						<p>＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿</p></c:if>
					</c:forEach>
				</div>
				<div id="force_tab01" class="cp_tabpanel">
					<c:forEach var="reviewList" items="${reviewList}">
						<c:if test="${reviewList.rating == 'three'}">
							<br>
							<h2>
								<font color=#dc143c>"${reviewList.title}"</font>
							</h2>
								 評価
									<c:if test="${reviewList.rating == 'three'}">
								<c:out value="☆☆☆" />
							</c:if>
							<c:if test="${reviewList.rating == 'two'}">
								<c:out value="☆☆" />
							</c:if>
							<c:if test="${reviewList.rating == 'one'}">
								<c:out value="☆" />
							</c:if>
								 投稿 ${reviewList.createDate}
								<c:if test="${reviewList.upDate != null}">
								<font color=orange> 更新${reviewList.upDate}</font>
							</c:if>
							<h5>
								<p>${reviewList.review}</p>
								<p></p>
							</h5>
							<a href="ReviewEditServlet?id=${reviewList.reviewId}">□編集</a>
							<a href="ReviewDeleteServlet?id=${reviewList.reviewId}">□削除</a>
						<p>＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿</p></c:if>
					</c:forEach>
				</div>

			</div>
		</div>
	</div>
	<section>
		<br>
 <div class="col-sm-12" align="center"><a href="MainServlet" class="cp_textlink01">TOPへ戻る</a></div>
 <div class="col-sm-12" align="center">arigatou-dance-odorow(yo)</div>
	</section>
	</section>


</body>
</html>