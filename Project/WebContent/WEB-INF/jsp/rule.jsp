<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="jsp/error.jsp"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>心得</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/rule.css" rel="stylesheet">


</head>

<body>
    <section>
        <p></p>
        <p>
            I　Q　３　に　な　っ　て　み　る　？</p>
    </section>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <font face="billing
            ">
                <div class="line">
                    <font face="BIZ UDMincho">
                        <h2>このサイトにおける三つの心得</h2>
                    </font>
                </div>
                <p></p>
                <font color=#ffdc00>
                    <h1> ・生きてるだけで天才
                        <p> ・息してるだけで<font face="doraneko_line">カワイイ</font>
                        </p>

                        <p> ・ええ！！？？生きてるだけですごいのに心臓も肺も筋肉も動かしてるの？？！なにそれ最強じゃん</p>
                    </h1>
                </font> <br>
                <font face="BIZ UDMincho">
                    <p> こんなの当たり前のことじゃな～い、と思われるかもしれません。</p>
                    <p>それって本当？日々を生きるというのは当たり前なのでしょうか？</p>
                    <p>当たり前かもしれませんね。
                    <h3>だから何？</h3>
                    </p>
                    <p> 当たり前を当たり前に出来ることをほめてはいけない法律でもあるのでしょうか？あるかもしれませんね。</p>
                    <p>
                    <h3>だから何？</h3>ここはあなただけの世界なんです。</p>
                    <p> 好きなだけ自分を褒めても責める人などいません</p>

                    <p>というわけで、以下のように自分を甘やかす練習をしてみましょう。</p>
                    <p></p>
                    <P></P>
                </font>
                <br>
                <p></p>
                <h4 id="sample">
                    <font color="#ffb6c1">例文）</font>
                    <p>
                    </p>
                    <font color="#b0c4de">
                        <p> 今日は、月曜日からお仕事だるかったけど出勤した自分えらい。</p>
                        <p> 天才かと思った。ちょっとお仕事でミスしたけど生きて帰ってきたから天才だよ。</p>
                        <p> 月曜日という魔物と戦った自分へのご褒美として帰りプリンも食べた。ご褒美にプリンを食べるとか自分かわいいかよ。最強。</p>
                        <p>これも自分が先月頑張って働いたから買えたんだよな～自分ありがとう。</p>
                    </font>
                </h4>
                <br>
                  <br>
                <p> <font color="#0073a8">”ポイント”</font></p>
                <p> </p>
                <p> 出来る前提で物事を考えない</p>
                 <p>私たちは全知全能の神じゃない</p>
                <p>弱く無知であることを自覚し受け入れよ</p>
                <p>しかし、それは自分を責める理由にはならない</p>
                <p><h5><font color="#e8383d">とりあえず、IQ3になろ</font></h5></p>
                <br>  <br>
        </div>
    </div>
    <section>
<br>
 <div class="col-sm-12" align="center"><a href="MainServlet" class="cp_textlink01">TOPへ戻る</a></div>
 <div class="col-sm-12" align="center">youkoso-kokohe-asobow(yo)</div>
    </section>
    </font>

</body></html>
