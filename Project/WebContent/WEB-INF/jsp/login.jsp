
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" errorPage="jsp/error.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<!-- オリジナルCSS読み込み -->

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">
</head>

<div class="col-sm-11">
	<font face="billing">
		<div class="title">I Q 3に な っ て み る ？</div>
	</font>
</div>
<div class="outer">
	<div class="inner"><div class="col-sm-12" align="center">
<p><c:if test="${errMsg != null}">
		<font face="BIZ UDMincho" color="#ff1a40"> ${errMsg}</font>
	</c:if></p>
		<font face="doraneko_line">
			<div class="login">ログインID</div>
		</font>
		<form method="post" action="LoginServlet">
			<div class="cp_iptxt">
				<label class="ef"> <input type="text" name="loginId" placeholder="loginId">
				</label>
			</div>
			<font face="doraneko_line">
				<div class="password">パスワード</div>
			</font>
			<div class="cp_iptxt">
				<label class="ef"> <input type="text" name="password" placeholder="password">
				</label>
			</div>
			<input src="image/login.png" class="image_button"  type="image">
		</form>
		        <a href="UserRegisterServlet" class="cp_textlink02"><h5>IQ3になってみたい。新規登録</h5></a>
	</div>
</div>
</div>
<body>




</body>
</html>
